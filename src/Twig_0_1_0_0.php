<?php

namespace ComposePress\Views\Engine;

use ComposePress\Core\Abstracts\Component_0_8_0_0;
use ComposePress\Views\Interfaces\ViewEngine;

/**
 * Class Twig_0_1_0_0
 *
 * @package ComposePress\Views\Engine
 * @property \ComposePress\Views\View_0_4_0_0 $parent
 */
class Twig_0_1_0_0 extends Component_0_8_0_0 implements ViewEngine {
	/**
	 * @var \Twig\Environment
	 */
	private $twig;

	/**
	 * @return bool
	 * @throws \ComposePress\Core\Exception\Plugin
	 */
	public function setup() {
		$this->parent->setup();
		$paths = [
			STYLESHEETPATH . DIRECTORY_SEPARATOR . $this->parent->overridable_path,
			TEMPLATEPATH . DIRECTORY_SEPARATOR . $this->parent->overridable_path,
		];
		$paths = array_filter( $paths, function ( $path ) {
			return $this->plugin->wp_filesystem->is_dir( $path );
		} );
		$this->twig = $this->create_object( '\\Twig\\Environment', [
			$this->create_object( '\\Twig\\Loader\\FilesystemLoader', [
				$paths,
				dirname( $this->parent->view_path ),
			] ),
			[ 'cache' => dirname( $this->plugin->plugin_file ) . DIRECTORY_SEPARATOR . 'view_cache' ],
		] );

		return true;
	}

	/**
	 * @return \Twig\Environment
	 */
	public function get_twig() {
		return $this->twig;
	}

	/**
	 * @param string $file
	 *
	 * @return bool
	 */
	public function is_overridable( $file ) {
		// We don't need to write to the file, so just open for reading.
		$fp = fopen( $file, 'r' );
		// Pull only the first 8kiB of the file in.
		$file_data = fread( $fp, 8192 );
		// PHP will close file handle, but we are good citizens.
		fclose( $fp );
		// Make sure we catch CR-only line endings.
		$file_data = str_replace( "\r", "\n", $file_data );
		$override  = false;
		if ( preg_match( '/^{#?\\s*Overrideable:\\s*([a-z]+)\\s*#?}/mi', $file_data, $matches ) ) {
			if ( in_array( strtolower( $matches[1] ), [ 'yes', 'true' ] ) ) {
				$override = true;
			}
			if ( in_array( strtolower( $matches[1] ), [ 'no', 'false' ] ) ) {
				$override = false;
			}
		}

		return $override;
	}

	/**
	 * @return string
	 */
	public function get_file_extension() {
		return 'twig';
	}

	/**
	 * @return bool
	 */
	public function is_buffered() {
		return false;
	}

	/**
	 * @param string $view
	 * @param array  $data
	 * @param bool   $return
	 *
	 * @return void|string
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function render( $view, $data = array(), $return = false ) {
		$path = dirname( $view );
		/**
		 * @var \Twig\Loader\FilesystemLoader $loader
		 */
		$loader = $this->twig->getLoader();
		if ( in_array( $path, $loader->getPaths() ) ) {
			$current_paths = array_unique( $loader->getPaths() );
			unset( $current_paths[ array_search( $path, $current_paths ) ] );
		}
		$loader->prependPath( $path );
		$plugin_dir = untrailingslashit( dirname( $this->plugin->plugin_file ) ) . DIRECTORY_SEPARATOR . 'views';
		if ( false !== strpos( $view, $plugin_dir ) ) {
			$view = str_replace( $plugin_dir, '', $view );
		}
		if ( false !== strpos( $view, $this->parent->overridable_path ) ) {
			/**
			 * @noinspection CallableParameterUseCaseInTypeContextInspection
			 */
			$view = explode( $this->parent->overridable_path, $view );
			$view = end( $view );
		}
		$output = $this->twig->render( $view, $data );
		if ( $return ) {
			/**
			 * @noinspection PhpInconsistentReturnPointsInspection
			 */
			return $output;
		}
		echo $output;
	}
}
