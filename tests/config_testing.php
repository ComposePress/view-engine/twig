<?php

/* @var $container \ComposePress\Dice\Dice */

$container = $container->addRule( '\Test_Plugin', [
	'shared' => true,
] );
$container = $container->addRule( 'ComposePress\Views\View_0_4_0_0',
	[
		'substitutions' => [
			'ComposePress\Views\Interfaces\ViewEngine' => '\ComposePress\Views\Engine\Twig_0_1_0_0',
		],
	] );
